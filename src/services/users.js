

export const getUsers = function () {
  return fetch('https://reqres.in/api/users', {method: 'GET'});
};

export const getUserById = function (id) {
  return fetch(`https://reqres.in/api/users/${id}`, {method: 'GET'});
};
